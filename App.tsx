import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import Atividade1Screen from './src/Atividade1.screen';
import Atividade2Screen from './src/Atividade2.screen';
import Atividade3Screen from './src/Atividade3.screen';
import Atividade4Screen from './src/Atividade4.screen';
import HomeScreen from './src/Home.screen';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Atividade 1" component={Atividade1Screen} />
        <Stack.Screen name="Atividade 2" component={Atividade2Screen} />
        <Stack.Screen name="Atividade 3" component={Atividade3Screen} />
        <Stack.Screen name="Atividade 4" component={Atividade4Screen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
