import React, {useState} from 'react';
import {
  Button,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';

const Atividade1Screen = () => {
  const [inputA, setInputA] = useState('');
  const [inputB, setInputB] = useState('');
  const [result, setResult] = useState('');

  const inputAChangeHandle = (e: string) => {
    if (e === '' || /^\d+$/.test(e)) {
      setInputA(e);
    }
  };
  const inputBChangeHandle = (e: string) => {
    if (e === '' || /^\d+$/.test(e)) {
      setInputB(e);
    }
  };

  const sumClickHandler = () => {
    const numA = parseInt(inputA);
    const numB = parseInt(inputB);
    setResult((numA + numB).toString());
  };

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <TextInput
          style={styles.btn}
          value={inputA}
          onChangeText={inputAChangeHandle}
        />
        <TextInput
          style={styles.btn}
          value={inputB}
          onChangeText={inputBChangeHandle}
        />
        <Button title="Somar" onPress={sumClickHandler} />
        <Text style={styles.text}>{result}</Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 8,
  },
  btn: {
    borderColor: '#aaa',
    borderWidth: 1,
    borderRadius: 20,
    padding: 16,
    marginBottom: 8,
    textAlign: 'center',
    color: '#000',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    margin: 16,
    color: '#000',
  },
});

export default Atividade1Screen;
