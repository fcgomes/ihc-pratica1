import Geolocation from '@react-native-community/geolocation';
import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {
  accelerometer,
  barometer,
  gyroscope,
  magnetometer,
  SensorTypes,
  setUpdateIntervalForType,
} from 'react-native-sensors';

setUpdateIntervalForType(SensorTypes.accelerometer, 400);

export const DataText = ({
  component,
  value,
}: {
  component: string;
  value: number;
}) => (
  <View>
    <Text
      style={{
        fontWeight: 'bold',
        color: '#000',
      }}>{`${component}: ${value.toFixed(2)}`}</Text>
  </View>
);

const Atividade2Screen = () => {
  const [shouldShow, setShouldShow] = useState(false);
  const [accelData, setAccelData] = useState({x: 0, y: 0, z: 0});
  const [gryoData, setGyroData] = useState({x: 0, y: 0, z: 0});
  const [magData, setMagData] = useState({x: 0, y: 0, z: 0});
  const [baroData, setBaroData] = useState({pressure: 0});
  const [geoData, setGeoData] = useState({lat: 0, long: 0, alt: 0});

  useEffect(() => {
    Geolocation.getCurrentPosition(info => {
      setGeoData({
        lat: info.coords.latitude,
        long: info.coords.longitude,
        alt: info.coords.altitude || 0,
      });
    });
  }, []);

  const accelSubscription = accelerometer.pipe().subscribe(
    data => {
      setAccelData(data);
    },
    error => {
      console.log(error);
    },
  );

  const gryoSubscription = gyroscope.pipe().subscribe(
    data => {
      setGyroData(data);
    },
    error => {
      console.log(error);
    },
  );

  const magSubscription = magnetometer.pipe().subscribe(
    data => {
      setMagData(data);
    },
    error => {
      console.log(error);
    },
  );

  const baroSubscription = barometer.pipe().subscribe(
    data => {
      setBaroData(data);
    },
    error => {
      console.log(error);
    },
  );

  setTimeout(() => {
    accelSubscription.unsubscribe();
    gryoSubscription.unsubscribe();
    magSubscription.unsubscribe();
    baroSubscription.unsubscribe();
  }, 1000);

  useEffect(() => {
    return () => {
      accelSubscription.unsubscribe();
      gryoSubscription.unsubscribe();
      magSubscription.unsubscribe();
      baroSubscription.unsubscribe();
    };
  }, []);

  const accelView = () => (
    <View style={{margin: 8}}>
      <Text
        style={{
          textAlign: 'center',
          marginBottom: 16,
          fontWeight: 'bold',
          fontSize: 20,
          color: '#000',
        }}>
        Acelerômetros
      </Text>
      <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
        <DataText component="x" value={accelData.x} />
        <DataText component="y" value={accelData.y} />
        <DataText component="z" value={accelData.z} />
      </View>

      <Text
        style={{
          textAlign: 'center',
          marginBottom: 16,
          fontWeight: 'bold',
          fontSize: 20,
          color: '#000',
        }}>
        Giroscópio
      </Text>
      <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
        <DataText component="x" value={gryoData.x} />
        <DataText component="y" value={gryoData.y} />
        <DataText component="z" value={gryoData.z} />
      </View>

      <Text
        style={{
          textAlign: 'center',
          marginBottom: 16,
          fontWeight: 'bold',
          fontSize: 20,
          color: '#000',
        }}>
        Magnetômetro
      </Text>
      <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
        <DataText component="x" value={magData.x} />
        <DataText component="y" value={magData.y} />
        <DataText component="z" value={magData.z} />
      </View>

      <Text
        style={{
          textAlign: 'center',
          marginBottom: 16,
          fontWeight: 'bold',
          fontSize: 20,
          color: '#000',
        }}>
        Barometro
      </Text>
      <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
        <Text style={{color: '#000'}}>{baroData.pressure}</Text>
      </View>

      <Text
        style={{
          textAlign: 'center',
          marginBottom: 16,
          fontWeight: 'bold',
          fontSize: 20,
          color: '#000',
        }}>
        Posição
      </Text>
      <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
        <DataText component="Lat" value={geoData.lat} />
        <DataText component="Long" value={geoData.lat} />
        <DataText component="Alt" value={geoData.alt} />
      </View>
    </View>
  );

  const positionView = () => <></>;

  return (
    <SafeAreaView style={styles.container}>
      {shouldShow ? positionView() : accelView()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 8,
  },
  btn: {
    borderColor: '#aaa',
    borderWidth: 1,
    borderRadius: 20,
    padding: 16,
    marginBottom: 8,
    textAlign: 'center',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    margin: 16,
  },
});

export default Atividade2Screen;
