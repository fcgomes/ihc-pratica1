import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {
  accelerometer,
  SensorTypes,
  setUpdateIntervalForType,
} from 'react-native-sensors';
import {filter, map} from 'rxjs/operators';

setUpdateIntervalForType(SensorTypes.accelerometer, 400);

export const Accelerometer = ({
  component,
  value,
}: {
  component: string;
  value: number;
}) => (
  <View>
    <Text
      style={{
        fontWeight: 'bold',
        color: '#000',
      }}>{`${component}: ${value.toFixed(2)}`}</Text>
  </View>
);

const Atividade3Screen = () => {
  const [shouldShow, setShouldShow] = useState(false);
  const [accelData, setAccelData] = useState({x: 0, y: 0, z: 0});

  const subscription = accelerometer.pipe().subscribe(
    data => {
      setAccelData(data);
    },
    error => {
      console.log(error);
    },
  );

  const subscription2 = accelerometer
    .pipe(
      map(({x, y, z}) => x + y + z),
      filter(speed => speed > 20),
    )
    .subscribe(
      speed => {
        console.log(`You moved your phone with ${speed}`);
        setShouldShow(true);
      },
      error => {
        console.log('The sensor is not available');
      },
    );

  setTimeout(() => {
    subscription.unsubscribe();
    subscription2.unsubscribe();
  }, 1000);

  useEffect(() => {
    return () => {
      subscription.unsubscribe();
      subscription2.unsubscribe();
    };
  }, []);

  const accelView = () => (
    <View style={{margin: 8}}>
      <Text
        style={{
          textAlign: 'center',
          marginBottom: 16,
          fontWeight: 'bold',
          fontSize: 20,
          color: '#000',
        }}>
        Acelerômetros
      </Text>
      <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
        <Accelerometer component="x" value={accelData.x} />
        <Accelerometer component="y" value={accelData.y} />
        <Accelerometer component="z" value={accelData.z} />
      </View>
    </View>
  );

  const positionView = () => (
    <>
      <Text style={{color: '#000'}}>Posição correta</Text>
    </>
  );

  return (
    <SafeAreaView style={styles.container}>
      {shouldShow ? positionView() : accelView()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 8,
  },
  btn: {
    borderColor: '#aaa',
    borderWidth: 1,
    borderRadius: 20,
    padding: 16,
    marginBottom: 8,
    textAlign: 'center',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    margin: 16,
  },
});

export default Atividade3Screen;
