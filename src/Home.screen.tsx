import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

const HomeScreen = () => {
  const navigation = useNavigation<any>();

  const atividades = [
    'Atividade 1',
    'Atividade 2',
    'Atividade 3',
    'Atividade 4',
  ];

  const clickHandler = (at: string) => {
    navigation.navigate(at);
  };

  return (
    <SafeAreaView style={styles.container}>
      {atividades.map(at => (
        <TouchableOpacity
          onPress={() => clickHandler(at)}
          key={at}
          style={styles.item}>
          <Text style={styles.text}>{at}</Text>
        </TouchableOpacity>
      ))}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 32,
  },
  item: {
    margin: 8,
    padding: 8,
    paddingHorizontal: 32,
    width: '100%',
    backgroundColor: '#fff',
    borderRadius: 20,
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000',
  },
});

export default HomeScreen;
