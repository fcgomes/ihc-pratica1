import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const Atividade2Screen = () => {
  const [text, setText] = useState('');
  const [shouldShow, setShouldShow] = useState(false);

  const textChangeHandler = (text: string) => {
    setText(text);
  };

  const clickHandler = () => {
    setShouldShow(true);
  };

  const inputView = () => (
    <View style={styles.inputContainer}>
      <TextInput
        style={styles.input}
        value={text}
        onChangeText={textChangeHandler}
      />
      <TouchableOpacity style={styles.btn}>
        <Text style={styles.btnText} onPress={clickHandler}>
          Send
        </Text>
      </TouchableOpacity>
    </View>
  );

  const showView = () => (
    <View>
      <Text style={styles.text}>{text}</Text>
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      {shouldShow ? showView() : inputView()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 8,
    width: '100%',
  },
  inputContainer: {
    width: '100%',
    flexDirection: 'row',
  },
  input: {
    flex: 4,
    borderColor: '#000',
    borderBottomWidth: 2,
    color: '#000',
  },
  btn: {
    flex: 1,
    borderColor: '#aaa',
    borderWidth: 1,
    borderRadius: 20,
    textAlign: 'center',
    justifyContent: 'center',
    margin: 8,
  },
  text: {
    color: '#000',
    fontSize: 20,
    textAlign: 'center',
    margin: 16,
  },
  btnText: {
    textAlign: 'center',
    color: '#000',
  },
});

export default Atividade2Screen;
